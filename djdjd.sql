create table akademik.strata(
id_strata smallint not null primary key,
singkat varchar (10),
strata varchar(45)
);

create table akademik.fakulutas(
id_fakulutas smallint not null primary key,
fakulutas varchar(45)
);

create table akademiks.jurusan(
id_jurusan smallint not null primary key,
id_fakultas smallint not null,
jurusan varchar(45),
foreign key(id_fakultas)
references akademik.fakultas(id_fakultas)
);

create table akademik.seleksi_masuk(
id_seleksi_masuk smallint not null primary key,
singkat varchar(12),seleksi_masuk(45)

);

create table akademiks.program_studi(
id_program_studi smallint not null primary key,
id_strata smallint,
id_jurusan smallint,
program_studi varchar(60),
foreign key (id_strata) references
akademiks.jurusan(id_jurusan)
);

create table akademiks.mahasiswa(
nim varchar(15) not null primary key,
id_seleksi_masuk smallint,
nama varchar(45),
angkatan smallint,
tanggal_lahir date,
kota_lahir date,
jenis_kelamin char(1) check
(jenis_kelamin in (‘m’,’f’)),
foreign key(id_program_studi) references
akademiks.program_studi(id_program_studi)
);

