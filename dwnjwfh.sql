select * from akademiks.fakulutas;
insert into akademiks.fakulutas(id_fakulutas,fakulutas)
values(1, 'Ekonomi & Bisnis');
insert into akademiks.fakulutas(id_fakulutas,fakulutas)
values(2, 'Ilmu Komputer');

select * from akademiks.jurusan;
insert into akademiks.jurusan(id_jurusan,id_fakulutas,jurusan)
values(21,2,'Informatika');
insert into akademiks.jurusan(id_jurusan,id_fakulutas,jurusan)
values(22,2,'Sistem Informasi');
insert into akademiks.jurusan(id_jurusan,id_fakulutas,jurusan)
values(23,2,'Teknik Komputer');

select * from akademiks.seleksi_masuk;
insert into akademiks.seleksi_masuk(id_seleksi_masuk,singkat,seleksi_masuk)
values(1,'SNMPTN','SELEKSI NASIONAL MAHASISWA PERGURUAN TINGGI NEGERI');
insert into akademiks.seleksi_masuk(id_seleksi_masuk,singkat,seleksi_masuk)
values(2,'SBMPTN','SELEKSI BERSAMA MAHASISWA PERGURUAN TINGGI NEGERI');

select * from akademiks.strata;
insert into akademiks.strata(id_strata,singkat,strata)
values(1,'D1','Diploma');
insert into akademiks.strata(id_strata,singkat,strata)
values(2,'S1','Sarjana');
insert into akademiks.strata(id_strata,singkat,strata)
values(3,'D1','Magister');

select * from akademiks.program_studi;
insert into akademiks.program_studi(id_program_studi,id_strata,id_jurusan,program_studi)
values (211,2,21,'Teknik Informatika');
insert into akademiks.program_studi(id_program_studi,id_strata,id_jurusan,program_studi)
values (212,2,21,'Teknik Komputer');
insert into akademiks.program_studi(id_program_studi,id_strata,id_jurusan,program_studi)
values (219,3,21,'Magister Ilmu Komputer');

select * from akademiks.mahasiswa;
insert into akademiks.mahasiswa values ('155150400',1,211,'JONI','2015','1997-01-01','Malang','f');
insert into akademiks.mahasiswa values ('155150401',2,212,'JONO','2015','1997-10-2','Situbondo','m');