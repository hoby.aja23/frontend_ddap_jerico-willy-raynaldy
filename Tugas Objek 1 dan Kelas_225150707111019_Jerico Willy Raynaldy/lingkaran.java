package com.company;
import java.util.*;
public class lingkaran {

    public double jarijari;
    private double keliling;
    private double luas;
    private double phi = 3.14;
    private double tinggi;
    static Scanner input = new Scanner(System.in);

    public double getLuas() {
        luas = phi * this.jarijari * this.jarijari;
        return luas;
    }

    public double getTinggi() {
        tinggi = 2 * this.jarijari;
        return tinggi;
    }

}