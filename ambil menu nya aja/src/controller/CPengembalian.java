/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import model.MPengembalian;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author ASUS
 */
public class CPengembalian {
    private List<MPengembalian> list = new ArrayList<>();
    private String[] kolom = {"Pengembali", "Penjaga", "Kode Peminjaman"};
    private DefaultTableModel tabelModel;
    
    
    public void addPengembalian(MPengembalian data){
        this.list.add(data);
        saveFile();
    }

    public List<MPengembalian> getList() {
        return list;
    }
    
    private void saveFile(){
        try{
            FileOutputStream writeData = new FileOutputStream("data/pengembalian.dat");
            ObjectOutputStream writeStream = new ObjectOutputStream(writeData);
            
            writeStream.writeObject(this.list);
            writeStream.flush();
            writeStream.close();
            
            }catch(IOException e){
                e.printStackTrace();
            }
    }
    
    public void readFile(){
        try{
            FileInputStream readData= new FileInputStream("data/pengembalian.dat");
            ObjectInputStream readStream = new ObjectInputStream(readData);
            
            this.list = (ArrayList<MPengembalian>) readStream.readObject();
            readStream.close();  
        }
        catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    
    public void editPengembalian (int index, MPengembalian data){
        this.list.set(index, data);
        saveFile();
    }
    
    public void deletePengembalian (int index){
        this.list.remove(index);
        saveFile();
    }
    
    public DefaultTableModel refresh(){
        tabelModel = new DefaultTableModel(kolom, 0);
        for(MPengembalian obj : list){
            tabelModel.addRow(obj.toArray());
        }
        return tabelModel;
    }
}