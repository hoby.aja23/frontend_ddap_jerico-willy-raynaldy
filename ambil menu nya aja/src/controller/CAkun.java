package controller;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.MAkun;

/**
 *
 * @author Allam
 */
public class CAkun {
    private static final String data = "data/users.dat";
    private List<MAkun> userList;
    
    public CAkun() {
        userList = new ArrayList<>();
        loadUsersFromFile();
    }

    public void saveUser(MAkun user) {
        try (FileOutputStream fileOut = new FileOutputStream(data);
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {

            userList.add(user);
            objectOut.writeObject(userList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MAkun findUser(String username) {
        for (MAkun user : userList) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    private void loadUsersFromFile() {
        try (FileInputStream fileIn = new FileInputStream(data);
             ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {

            userList = (ArrayList<MAkun>) objectIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            // Ignore if file does not exist or if it is the first run
        }
    }
}