/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.MBuku;

/**
 *
 * @author WINDOWS 11
 */
public class CBuku {
    private List<MBuku> list = new ArrayList<>();
    private String[] kolom = {"Kode Buku", "Judul"};
    private DefaultTableModel tabelModel;
    
    public void addBuku(MBuku data){
        this.list.add(data);
        saveFile();
    }
    
    public List<MBuku> getList() {
        return list;
    }
    
    private void saveFile(){
        try{
            FileOutputStream writeData = new FileOutputStream("data/buku.dat");
            ObjectOutputStream writeStream = new ObjectOutputStream(writeData);
            
            writeStream.writeObject(list);
            writeStream.flush();
            writeStream.close();
            
        }catch(IOException e){
        }
    }
    
    public void readFile(){
        try{
            FileInputStream readData = new FileInputStream("data/buku.dat");
            ObjectInputStream readStream = new ObjectInputStream(readData);

            this.list = (ArrayList<MBuku>) readStream.readObject();
            readStream.close();
            System.out.println(this.list.toString());
        
        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
       
    }
    
    public void editBuku(int index, MBuku data){
        this.list.set(index, data);
        saveFile();
    }
    
     public void deleteBuku(int index){
        this.list.remove(index);
        saveFile();
    }


    
    public DefaultTableModel refresh(){
        tabelModel = new DefaultTableModel (kolom, 0);
        for (MBuku obj : list){
            tabelModel.addRow(obj.toArray());
        }
        return tabelModel;
    }
    
    
}
