/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.MPeminjaman;

/**
 *
 * @author WINDOWS 11
 */
public class CPeminjaman {
    private List<MPeminjaman> list = new ArrayList<>();
    private String[] kolom = {"Peminjam", "Petugas", "Kode Peminjaman"};
    private DefaultTableModel tabelModel;
    
    public void addPeminjaman(MPeminjaman data){
        this.list.add(data);
        saveFile();
    }
    
    public List<MPeminjaman> getList() {
        return list;
    }
    
    private void saveFile(){
        try{
            FileOutputStream writeData = new FileOutputStream("data/peminjaman.dat");
            ObjectOutputStream writeStream = new ObjectOutputStream(writeData);
            
            writeStream.writeObject(list);
            writeStream.flush();
            writeStream.close();
            
        }catch(IOException e){
        }
    }
    
    public void readFile(){
        try{
            FileInputStream readData = new FileInputStream("data/peminjaman.dat");
            ObjectInputStream readStream = new ObjectInputStream(readData);

            this.list = (ArrayList<MPeminjaman>) readStream.readObject();
            readStream.close();
            System.out.println(this.list.toString());
        
        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
       
    }
    
    public void editPeminjaman(int index, MPeminjaman data){
        this.list.set(index, data);
        saveFile();
    }
    
     public void deletePeminjaman(int index){
        this.list.remove(index);
        saveFile();
    }
    
    public DefaultTableModel refresh(){
        tabelModel = new DefaultTableModel (kolom, 0);
        for (MPeminjaman obj : list){
            tabelModel.addRow(obj.toArray());
        }
        return tabelModel;
    }
    
}
