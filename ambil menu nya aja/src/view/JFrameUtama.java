/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package view;

/**
 *
 * @author WINDOWS 11
 */
public class JFrameUtama extends javax.swing.JFrame {
    JInternalFrameBuku x = new JInternalFrameBuku();
    

    /**
     * Creates new form JFrameUtama
     */
    public JFrameUtama() {
        initComponents();
        jDesktopPaneUtama.add(x);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPaneUtama = new javax.swing.JDesktopPane();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuItemLogOut = new javax.swing.JMenuItem();
        jMenuManajemen = new javax.swing.JMenu();
        jMenuItemBuku = new javax.swing.JMenuItem();
        jMenuItemPeminjaman = new javax.swing.JMenuItem();
        jMenuItemPengembalian = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jButton1.setText("jButton1");

        jDesktopPaneUtama.setLayer(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPaneUtamaLayout = new javax.swing.GroupLayout(jDesktopPaneUtama);
        jDesktopPaneUtama.setLayout(jDesktopPaneUtamaLayout);
        jDesktopPaneUtamaLayout.setHorizontalGroup(
            jDesktopPaneUtamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDesktopPaneUtamaLayout.createSequentialGroup()
                .addContainerGap(324, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(230, 230, 230))
        );
        jDesktopPaneUtamaLayout.setVerticalGroup(
            jDesktopPaneUtamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPaneUtamaLayout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jButton1)
                .addContainerGap(274, Short.MAX_VALUE))
        );

        jMenuFile.setText("File");

        jMenuItemLogOut.setText("Log Out");
        jMenuItemLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLogOutActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemLogOut);

        jMenuBar1.add(jMenuFile);

        jMenuManajemen.setText("Manajemen");

        jMenuItemBuku.setText("Buku");
        jMenuItemBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemBukuActionPerformed(evt);
            }
        });
        jMenuManajemen.add(jMenuItemBuku);

        jMenuItemPeminjaman.setText("Peminjaman");
        jMenuItemPeminjaman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPeminjamanActionPerformed(evt);
            }
        });
        jMenuManajemen.add(jMenuItemPeminjaman);

        jMenuItemPengembalian.setText("Pengembalian");
        jMenuItemPengembalian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPengembalianActionPerformed(evt);
            }
        });
        jMenuManajemen.add(jMenuItemPengembalian);

        jMenuBar1.add(jMenuManajemen);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPaneUtama)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPaneUtama)
        );

        setSize(new java.awt.Dimension(643, 393));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemBukuActionPerformed
        // TODO add your handling code here:   
        x.setVisible(true);  
    }//GEN-LAST:event_jMenuItemBukuActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowOpened

    private void jMenuItemPeminjamanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPeminjamanActionPerformed
        // TODO add your handling code here:
        JInternalFramePeminjaman y = new JInternalFramePeminjaman();
        jDesktopPaneUtama.add(y);
        y.setVisible(true);
    }//GEN-LAST:event_jMenuItemPeminjamanActionPerformed

    private void jMenuItemLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLogOutActionPerformed
        // TODO add your handling code here:
        JFrameLogin x = new JFrameLogin();
        this.dispose();
        x.setVisible(true);
    }//GEN-LAST:event_jMenuItemLogOutActionPerformed

    private void jMenuItemPengembalianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPengembalianActionPerformed
        // TODO add your handling code here:
        JInternalFramePengembalian z = new JInternalFramePengembalian();
        jDesktopPaneUtama.add(z);
        z.setVisible(true);
    }//GEN-LAST:event_jMenuItemPengembalianActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameUtama().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JDesktopPane jDesktopPaneUtama;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenuItem jMenuItemBuku;
    private javax.swing.JMenuItem jMenuItemLogOut;
    private javax.swing.JMenuItem jMenuItemPeminjaman;
    private javax.swing.JMenuItem jMenuItemPengembalian;
    private javax.swing.JMenu jMenuManajemen;
    // End of variables declaration//GEN-END:variables
}
