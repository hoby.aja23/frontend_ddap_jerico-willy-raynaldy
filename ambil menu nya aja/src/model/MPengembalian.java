/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class MPengembalian implements Serializable{
    public String pengembali;
    public String penjaga;
    public String kodePeminjaman;

    public MPengembalian(String pengembali, String penjaga, String kodePeminjaman) {
        this.pengembali = pengembali;
        this.penjaga = penjaga;
        this.kodePeminjaman = kodePeminjaman;
    }
   
    public String [] toArray(){
       String[] data = {pengembali, penjaga, kodePeminjaman};
       return data;
   }
}

