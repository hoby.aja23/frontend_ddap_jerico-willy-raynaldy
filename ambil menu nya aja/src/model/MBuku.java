/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.io.Serializable;

/**
 *
 * @author WINDOWS 11
 */
public class MBuku implements Serializable{
    public String kodeBuku;
    public String judulBuku;

    public MBuku(String kodeBuku, String judulBuku) {
        this.kodeBuku = kodeBuku;
        this.judulBuku = judulBuku;
    }
    
    public String[] toArray(){
        String[] data = {kodeBuku, judulBuku};
        return data;
    }
    
}
