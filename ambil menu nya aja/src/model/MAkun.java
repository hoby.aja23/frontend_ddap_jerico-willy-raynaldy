package model;

import java.io.Serializable;

/**
 *
 * @author Allam
 */
public class MAkun implements Serializable {
    private String username;
    private String password;

    public MAkun(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
