/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.io.Serializable;

/**
 *
 * @author WINDOWS 11
 */
public class MPeminjaman implements Serializable{
    public String peminjam;
    public String petugas;
    public String kodePeminjaman;

    
    public MPeminjaman(String peminjam, String petugas, String kodePeminjaman) {
        this.peminjam = peminjam;
        this.petugas = petugas;
        this.kodePeminjaman = kodePeminjaman;
    }
    
    public String[] toArray(){
        String[] data = {peminjam, petugas, kodePeminjaman};
        return data;
    }
}
