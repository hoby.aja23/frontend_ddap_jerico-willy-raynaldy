package model;

public class ModelLogin {
    private String username;
    private String password;

    public ModelLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
